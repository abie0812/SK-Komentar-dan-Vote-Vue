Vue.component('comments', {
    template: '#comment-template',
    props   : ['comment'], // menandakan bahwa mengoper nilai
    data    : function() {
        return {
            plus : false, 
            minus: false
        }
    },
    methods: {
        sundul: function() { // upvote
            this.plus = !this.plus // true
            this.minus= false
        }, 
        bata: function() { // downvote
            this.minus= !this.minus // true
            this.plus = false
        }
    },
    computed: {
        coto: function() {
            if (this.plus)
                return this.comment.coto + 1
            else if (this.minus)
                return this.comment.coto - 1
            else 
                return this.comment.coto
        }
    }
})


var vm = new Vue({
    el: "#app", 
    data: {
        comments: [
            { body: 'komentar pertama saya', time: '26-06-2018', coto: 10 },
            { body: 'mangstap gans', time: '26-06-2018', coto: 0 },
            { body: 'oki doki', time: '26-06-2018', coto: 5 },
        ]
    }, 
    methods: {
        postComment: function() {
            this.comments.push(
                { body: this.comment_text, time: '26-06-2018', coto: 0 }
            )
            this.comment_text = ''
        }        
    }
})