## Sistem Komentar Sederhana
1. User dapat melakukan upvote dan downvote.
   Menggunakan boolean untuk mengontrol upvote dan downvote.
   Apabila User hanya bisa melakukan upvote atau downvote satu kali saja, 
   untuk memberi tanda maka si button upvote ataupun downvote dibuat disable indikatornya
   adalah warna merah.
2. User dapat menambah komentar baru.

## Catatan: 
1. Data masih statis.
2. Sistem yang dibuat sederhana.

## Point penting : 
Menggunakan sistem komentar dan vote sederhana, memainkan boolean untuk sistem votenya.
